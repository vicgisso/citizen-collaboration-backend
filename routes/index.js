import express from "express";
import userRouter from "./user";
import issueRouter from "./issue"
// import { issueRouter } from "./issue";
// import { suggestionRouter } from "./suggestion";
// import { infoRouter } from "./info";

const router = express.Router();

router.use('/user', userRouter);
router.use('/issue', issueRouter);

export default router;