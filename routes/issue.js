import express from 'express';

const router = express.Router();

router.post('/create', (req, res) => {
  res.send('create issue');
});

router.post('/update', (req, res) => {
  res.send('update issue');
});

router.get('/cancel', (req, res) => {
  res.send('cancel issue');
});

router.get('/delete', (req, res) => {
  res.send('delete issue');
});

router.get('/warn', (req, res) => {
  res.send('warn issue');
});

router.post('/list', (req, res) => {
  res.send('list issues');
});

export default router;