import express from "express";

const router = express.Router();

router.post('/create', (req, res) => {
  res.send('create user');
});

router.get('/ban', (req, res) => {
  res.send('ban user');
});
router.post('/update', (req, res) => {
  res.send('update user');
});

router.get('/active', (req, res) => {
  res.send('active user');
});
router.post('/delete', (req, res) => {
  res.send('delete user');
});

export default router;